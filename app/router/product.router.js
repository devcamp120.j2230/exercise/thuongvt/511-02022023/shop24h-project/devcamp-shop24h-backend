//Khai báo thư viện express
const express = require("express")
//Khai báo Middleware 
const productMiddleware = require("../middleware/product.middleware");
//Khai báo controller
const  productController = require("../controller/product.controller")

//Khai báo router
const productRouter = express.Router();
//Sử dụng router
productRouter.post("/products",productMiddleware.Shop24hMiddleware,productController.createProduct),
productRouter.get("/products",productMiddleware.Shop24hMiddleware,productController.getAllProduct),
productRouter.get("/products/:productId", productMiddleware.Shop24hMiddleware,productController.getProductById);
productRouter.put("/products/:productId",productMiddleware.Shop24hMiddleware,productController.updateProduct);
productRouter.delete("/products/:productId",productMiddleware.Shop24hMiddleware,productController.deleteProduct)

// exprot Router thành modul
module.exports = {productRouter}