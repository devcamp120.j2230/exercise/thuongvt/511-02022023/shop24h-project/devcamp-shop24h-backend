//Khai báo thư viện express
const express = require("express")
//Khai báo Middleware 
const orderMiddleware = require("../middleware/order.middleware");
//Khai báo controller 
const orderController = require("../controller/order.controller");
//Khai báo router
const orderRouter = express.Router();
//Sử dụng router
orderRouter.post("/customers/:customerId/orders",orderMiddleware.Shop24hMiddleware,orderController.createOrderOfCustomer);
orderRouter.get("/orders",orderMiddleware.Shop24hMiddleware,orderController.getAllOrder);
orderRouter.get("/customers/:customerId/orders",orderMiddleware.Shop24hMiddleware,orderController.getAllOrderOfCustomer);
orderRouter.get("/orders/:orderId",orderMiddleware.Shop24hMiddleware,orderController.getOrderById);
orderRouter.put("/orders/:orderId",orderMiddleware.Shop24hMiddleware,orderController.updateOrder);
orderRouter.delete("/orders/:orderId",orderMiddleware.Shop24hMiddleware,orderController.deleteOrder);
//exprot router thành modul
module.exports = {orderRouter}