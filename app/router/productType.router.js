//Khai báo thư viện express
const express = require("express")
//Khai báo Middleware 
const productTypeMiddleware = require("../middleware/productType.middleware");
//Nơi khai báo controller
const {
    CreateProductType,
    GetAllProductType,
    GetProductTypeByID,
    UpdateProductType,
    DeleteProductType
} = require("../controller/productType.controller")

//Nơi tạo Router
const productTypeRouter = express.Router();
//Nơi sử dụng các Router
productTypeRouter.post("/productypes",productTypeMiddleware.Shop24hMiddleware,CreateProductType);
productTypeRouter.get("/productypes",productTypeMiddleware.Shop24hMiddleware,GetAllProductType);
productTypeRouter.get("/productypes/:productypesId",productTypeMiddleware.Shop24hMiddleware,GetProductTypeByID);
productTypeRouter.put("/productypes/:productypesId",productTypeMiddleware.Shop24hMiddleware,UpdateProductType);
productTypeRouter.delete("/productypes/:productypesId",productTypeMiddleware.Shop24hMiddleware,DeleteProductType)

//Exprot router
module.exports = {productTypeRouter}
