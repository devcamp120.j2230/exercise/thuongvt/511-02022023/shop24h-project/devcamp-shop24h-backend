//Khai báo thư viện express
const express = require("express")
//Khai báo Middleware 
const customerMiddleware = require("../middleware/customer.middleware");
//Khai báo controller
const customerController = require("../controller/customer.controller")
//Khai báo ruoter
const customerRouter = express.Router();
//Sử dụng router

customerRouter.post("/customers",customerMiddleware.Shop24hMiddleware,customerController.createCustomer);
customerRouter.get("/customers",customerMiddleware.verifyToken,customerController.getAllCustomer);
customerRouter.get("/customers/:customerId",customerMiddleware.Shop24hMiddleware,customerController.getCustomerById)
customerRouter.put("/customers/:customerId",customerMiddleware.Shop24hMiddleware, customerController.updateCustomer);
customerRouter.delete("/customers/:customerId",customerMiddleware.verifyTokenAndAdmin, customerController.deleteCustomer)



//exprot router thành modul
module.exports = {customerRouter}