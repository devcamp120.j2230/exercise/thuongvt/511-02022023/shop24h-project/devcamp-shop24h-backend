//Khai báo thư viện express
const express = require("express")
//Khai báo Middleware 
const logInMiddleware = require ("../middleware/login.middleware")
const customerMiddleware= require("../middleware/customer.middleware")
//Khai báo controller
const logInController = require ("../controller/login.controller")



//Khai báo ruoter
const logInRouter = express.Router();

//Sử dụng router
logInRouter.post("/login",logInMiddleware.Shop24hMiddleware, logInController.loginUser )
logInRouter.post("/register", logInMiddleware.Shop24hMiddleware,logInController.registerCustomer)
logInRouter.post("/logOut",customerMiddleware.verifyToken, logInController.logOut)// đi qua middlewear phải login rồi mới log out
//exprot router thành modul
module.exports={logInRouter}