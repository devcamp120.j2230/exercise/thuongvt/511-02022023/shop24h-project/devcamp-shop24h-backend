//Khai báo thư viện express
const express = require("express")
//Khai báo Middleware 
const orderDetailMiddleware = require("../middleware/orderDetail.middleware");
//Khai báo controller 
const orderDetailController = require("../controller/orderDetail.controller");
//Khia báo router
const orderDetailRouter = express.Router();
//Sử dụng router
orderDetailRouter.post("/orders/:orderId/orderDetails",orderDetailMiddleware.Shop24hMiddleware,orderDetailController.createOrderDetailOfOrder);
orderDetailRouter.get("/orders/:orderId/orderDetails",orderDetailMiddleware.Shop24hMiddleware,orderDetailController.getAllOrderDetailOfOrder)
orderDetailRouter.get("/orderDetails/:orderDetailId",orderDetailMiddleware.Shop24hMiddleware,orderDetailController.getOrderDetailById);
orderDetailRouter.put("/orderDetails/:orderDetailId",orderDetailMiddleware.Shop24hMiddleware,orderDetailController.updateOrderDetail);
orderDetailRouter.delete("/orderDetails/:orderDetailId",orderDetailMiddleware.Shop24hMiddleware,orderDetailController.deleteOrderDetail)
//exprot router thành modul
module.exports = {orderDetailRouter}