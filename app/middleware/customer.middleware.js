const jwt = require("jsonwebtoken")

const Shop24hMiddleware = (req, res, next) => {
    console.log(`method: ${req.method} = URL: ${req.url} - Time: ${new Date()}`);

    next();
}

const verifyToken = (req, res, next) => {
    const token = req.headers.token;
    if (token) {
        const accessToken = token.split(" ")[1];
        jwt.verify(accessToken, "mk", (err, user) => {
            if (err) {
              return  res.status(403).json("Token is not valid");
            }
            req.user = user;
            next();
        })
    }
    else {
      return  res.status(401).json("you are not authenticated")
    }
}

const verifyTokenAndAdmin = (req, res, next) => {
    verifyToken(req,res,()=>{
        //nêu id của token giử về trùng với id của người dùng hoặc user của admin thì được phép xóa người dùng
        if(req.user.id==req.params.id||req.user.admin){
            next();
        }
        else {
          return  res.status(403).json("you are not delete")
        }
    })



}

module.exports = { Shop24hMiddleware, verifyToken, verifyTokenAndAdmin };