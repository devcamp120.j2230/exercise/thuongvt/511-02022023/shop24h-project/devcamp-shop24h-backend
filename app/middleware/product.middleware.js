const Shop24hMiddleware = (req,res,next) =>{
    console.log(`method: ${req.method} = URL: ${req.url} - Time: ${new Date()}`);

    next();
}

module.exports = {Shop24hMiddleware};