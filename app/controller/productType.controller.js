// khai báo thư viện mongoose
const mongoose = require("mongoose");
// Khai báo model
const ProductTypeModel = require("../model/productType.model");
// CreateProductType 
const CreateProductType = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let body = req.body
    //B2 Kiểm tra dữ liệu
    if(!body.name){
        return res.status(400).json({
            message: "Dữ liệu name chưa chính xác"
        })
    }
    if(!body.description){
        return res.status(400).json({
            message: " Dữ liệu description chưa chính xác"
        })
    }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    let newProductTypeData = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description
    }
    ProductTypeModel.create(newProductTypeData,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(201).json({
                message: "Tạo mới thành công",
                ProductType: data
        })
    })
}
// GetAllProductType
const GetAllProductType = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    //B2 Kiểm tra dữ liệu
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    ProductTypeModel.find((err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message:"lấy toàn bộ dữ liệu thành công",
            ProductType: data
        })
    })
}
// GetProductTypeByID
const GetProductTypeByID = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let Id = req.params.productypesId
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
          message: 'id không hợp lệ'
        })
      }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    ProductTypeModel.findById(Id,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message:"Lấy dữ liệu thành công thông qua ID",
            ProductType: data
        })
    })
}
// UpdateProductType
const UpdateProductType = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let Id = req.params.productypesId;
    let body = req.body
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
          message: 'id không hợp lệ'
        })
      }
    if(!body.name){
        return res.status(400).json({
            message: "Dữ liệu name chưa chính xác"
        })
    }
    if(!body.description){
        return res.status(400).json({
            message: " Dữ liệu description chưa chính xác"
        })
    }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    let newProductTypeData = {
        name: body.name,
        description: body.description
    }
    ProductTypeModel.findByIdAndUpdate(Id,newProductTypeData,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message:"Sửa dữ liệu thành công thông qua ID",
            ProductType: data
        })
    })
}
// DeleteProductType
const DeleteProductType = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let Id = req.params.productypesId;
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
          message: 'id không hợp lệ'
        })
      }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    ProductTypeModel.findByIdAndDelete(Id,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message: "Xóa dư liệ thành công qua Id",
            Data: data
        })
    })
}
//Export thư viện controller thành một modul
module.exports = {
    CreateProductType,
    GetAllProductType,
    GetProductTypeByID,
    UpdateProductType,
    DeleteProductType
}