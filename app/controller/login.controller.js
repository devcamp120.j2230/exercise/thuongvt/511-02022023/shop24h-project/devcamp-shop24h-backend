//Khai báo thư viện mongose
const mongoose = require("mongoose");
//Khai báo thư viện model
const customerModel = require("../model/customer.model");
const bcrypt = require('bcrypt');
const jwt = require(`jsonwebtoken`)


// login
const loginUser = async (req, res) => {
    let body = req.body

    if (!body.email) {
        return res.status(400).json({
            message: "Email không có"
        })
    }
    if (!body.password) {
        return res.status(400).json({
            message: "password không được để trống"
        })
    }
    customerModel.
        findOne({
            email: body.email
        })
        .exec(
            async (error, data) => {
                if (error) {
                    return res.status(500).json({
                        message: `Loi 500: ${error.message}.`
                    });
                }
                else {
                    if (data !== null) {
                        var checkPassWord = await bcrypt.compareSync(body.password, data.password)
                        console.log(checkPassWord)

                        var accessToken = jwt.sign({
                            _id: data._id,
                            admin: data.admin
                        },
                            "mk",
                            { expiresIn: "24h" }
                        )
                        var refreshToken = jwt.sign({
                            _id: data._id,
                            admin: data.admin
                        },
                            "mt",
                            { expiresIn: "360d" }
                        )
                        
                        if (checkPassWord) {
                            // add dữ liệu vào cookie
                            res.cookie("refreshToken", refreshToken, {
                                httpOnly:true,
                                secure:false,
                                path:"/",
                                sameSite:"strict"
                            })
                            console.log(accessToken, data)
                            return res.status(200).json({
                                message: "Tìm dữ liệu customer thành công",
                                accessToken: accessToken,
                                // refreshToken: refreshToken,
                                Data: data
                            })
                        }
                        else {
                            return res.status(400).json({
                                message: `Sai Password.`,
                            });

                        }

                    }
                    else {
                        return res.status(400).json({
                            message: `Sai Email`,
                        });
                    }
                }
            }
        )
}

// registerCustomer 
const registerCustomer = async (req, res) => {
    //B1 chuẩn bị dữ liệu
    let body = req.body
    console.log(body)
    //B2 Kiểm tra dữ liệu
    if (!body.firstName) {
        return res.status(400).json({
            message: "firstName không có"
        })
    }
    if (!body.lastName) {
        return res.status(400).json({
            message: "Last Name không có"
        })
    }
    if (!body.phone) {
        return res.status(400).json({
            message: "Phone không có"
        })
    }
    if (!body.email) {
        return res.status(400).json({
            message: "Email không có"
        })
    }
    if (!body.street) {
        return res.status(400).json({
            message: "street không có"
        })
    }
    if (!body.city) {
        return res.status(400).json({
            message: "City không có"
        })
    }
    if (!body.province) {
        return res.status(400).json({
            message: "province không có"
        })
    }
    if (!body.wards) {
        return res.status(400).json({
            message: "wards  không có"
        })
    }
    if (!body.password) {
        return res.status(400).json({
            message: "password không được để trống"
        })
    }

    const salt = await bcrypt.genSaltSync(8)
    const hash = await bcrypt.hashSync(body.password, salt);
    //B3 Gọi model thực hiện nghiệp vụ
    let newCustomerData = {
        _id: mongoose.Types.ObjectId(),
        firstName: body.firstName,
        lastName: body.lastName,
        phone: body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        province: body.province,
        wards: body.wards,
        image: body.image,
        street: body.street,
        password: hash
    }
    customerModel.create(newCustomerData, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(201).json({
            message: "Tạo dữ liệu customer thành công",
            Data: data
        })
    })
}

// logout
const logOut = async(req,res)=>{
    res.clearCookie("refreshToken")
    res.status(200).json({
        message: "LogOut thành công"
    })
}
//exprot thành modul
module.exports = {
    loginUser,
    registerCustomer,
    logOut
}