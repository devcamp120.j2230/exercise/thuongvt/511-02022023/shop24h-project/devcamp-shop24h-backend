//Khai báo thư viện mongose
const mongoose = require("mongoose");

//Khai báo thư viện model
const orderDetailModel = require("../model/orderDetail.model");
const orderModel = require("../model/order.model");
// createOrderDetailOfOrder
const createOrderDetailOfOrder = (req, res) => {
    //B1 Chuẩn bị dữ liệu
    let Id = req.params.orderId
    let body = req.body
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            "status": "Bad Request",
            "Message": "orderId is not valid!"
        })
    }
    if (!body.quantity) {
        return res.status(400).json({
            message: "quantity chưa có giá trị!"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(body.product)) {
        return res.status(400).json({
            "status": "Bad Request",
            "Message": " product is not valid!"
        })
    }
    //B3 Gọi model thực hiện bào toán tạo mới dữ liệu
    let newOrderDetail = {
        _id: mongoose.Types.ObjectId(),
        quantity: body.quantity,
        product: body.product
    }
    orderDetailModel.create(newOrderDetail, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error2",
                message: err.message
            })
        }
        orderModel.findByIdAndUpdate(Id, { $push: { orderDetails: data._id } }, (err1, data1) => {
            if (err1) {
                return res.status(500).json({
                    status: "Internal server error1",
                    message: err1.message
                })
            }
            return res.status(201).json({
                status: "Create orderDetail successfully",
                data: data
            })
        })
    })
}
// getAllOrderDetailOfOrder
const getAllOrderDetailOfOrder = (req, res) => {
    //B1 Chuẩn bị dữ liệu
    let Id = req.params.orderId
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            "status": "Bad Request",
            "Message": "customerId is not valid!"
        })
    }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    orderModel.findById(Id).populate("orderDetails").exec((err, data) => {
        console.log(data, err)
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }

        return res.status(200).json({
            status: "Get orderDetail successfully",
            Data: data.orderDetails
        })
    })
}
// getOrderDetailById 
const getOrderDetailById = (req, res) => {
    //B1: thu thập dữ liệu từ req
    let Id = req.params.orderDetailId
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            "status": "Bad Request",
            "Message": "customerId is not valid!"
        })
    }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    orderDetailModel.findById(Id, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }

        return res.status(200).json({
            status: "Get orderDetail successfully",
            Data: data
        })
    })
}
// updateOrderDetail 
const updateOrderDetail = (req, res) => {
    //B1: thu thập dữ liệu từ req
    let Id = req.params.orderDetailId;
    let body = req.body
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            "status": "Bad Request",
            "Message": "customerId is not valid!"
        })
    }
    if (!body.quantity) {
        return res.status(400).json({
            message: "quantity is not valid!"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(body.product)) {
        return res.status(400).json({
            "status": "Bad Request",
            "Message": " product is not valid!"
        })
    }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    let newOrderDetailUpdate = {
        quantity: body.quantity,
        product: body.product
    }
    orderDetailModel.findByIdAndUpdate(Id, newOrderDetailUpdate, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }

        return res.status(200).json({
            status: "update orderDetail successfully",
            Data: data
        })
    })
}
// deleteOrderDetail
const deleteOrderDetail = (req, res) => {
    //B1: thu thập dữ liệu từ req
    let Id = req.params.orderDetailId;
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            "status": "Bad Request",
            "Message": "customerId is not valid!"
        })
    }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    orderDetailModel.findByIdAndDelete(Id, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }

        return res.status(200).json({
            status: "delete orderDetail successfully",
            Data: data
        })
    })
}

//exprot thành modul
module.exports = {
    createOrderDetailOfOrder,
    getAllOrderDetailOfOrder,
    getOrderDetailById,
    updateOrderDetail,
    deleteOrderDetail
}