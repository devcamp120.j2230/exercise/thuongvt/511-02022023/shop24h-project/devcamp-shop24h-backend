//Khai báo thư viện mongoose
const mongoose = require("mongoose");
//Khái báo thư viện model
const ProductModel = require("../model/product.model");
// createProduct
const createProduct = (req, res) => {
    //B1 chuẩn bị dữ liệu
    let body = req.body
    //B2 Kiểm tra dữ liệu
    if (!body.name) {
        return res.status(400).json({
            message: " Dữ liệu name chưa chính xác"
        })
    }
    if (!body.description) {
        return res.status(400).json({
            message: "Dữ liệu description chưa chính xác"
        })
    }
    if (!body.imageUrl) {
        return res.status(400).json({
            message: "Dữ liệu imageUrl chưa chính xác"
        })
    }
    if (!body.buyPrice) {
        return res.status(400).json({
            message: "Dữ liệu buyPrice chưa chính xác"
        })
    }
    if (!body.promotionPrice) {
        return res.status(400).json({
            message: "Dữ liệu promotionPrice chưa chính xác"
        })
    }
    if (!body.amount) {
        return res.status(400).json({
            message: "Dữ liệu amount chưa chính xác"
        })
    }
    //B3 Gọi model thực hiện nghiệp vụ
    let newProductData = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
	    description: body.description,
	    type: body.type,
	    imageUrl: body.imageUrl,
	    buyPrice: body.buyPrice,
	    promotionPrice: body.promotionPrice,
	    amount: body.amount
    }
    ProductModel.create(newProductData,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(201).json({
            message:"Tạo dữ liệu product thành công",
            Data: data
        })
    })
}
// getAllProduct 
const getAllProduct = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let productsType = req.query.Type
    let condition = {}
    //B2: validate dữ liệu
    if(productsType){
        condition.type = productsType
    }
    //B2 Kiểm tra dữ liệu
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    ProductModel.find(condition).limit().exec((err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message:"tải toàn bộ dữ liệu thành công",
            Data: data
        })
    })
}
// getProductById
const getProductById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let Id = req.params.productId
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
          message: 'id không hợp lệ'
        })
      }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    ProductModel.findById(Id,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message: "Lấy dữ liệu thành công thông qua Id",
            Data: data
        })
    })
}
// updateProduct
const updateProduct = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let Id = req.params.productId;
    let body = req.body
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
          message: 'id không hợp lệ'
        })
      }
      if (!body.name) {
        return res.status(400).json({
            message: " Dữ liệu name chưa chính xác"
        })
    }
    if (!body.description) {
        return res.status(400).json({
            message: "Dữ liệu description chưa chính xác"
        })
    }
    if (!body.imageUrl) {
        return res.status(400).json({
            message: "Dữ liệu imageUrl chưa chính xác"
        })
    }
    if (!body.buyPrice) {
        return res.status(400).json({
            message: "Dữ liệu buyPrice chưa chính xác"
        })
    }
    if (!body.promotionPrice) {
        return res.status(400).json({
            message: "Dữ liệu promotionPrice chưa chính xác"
        })
    }
    if (!body.amount) {
        return res.status(400).json({
            message: "Dữ liệu amount chưa chính xác"
        })
    }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    let newProductDataUpdate = {
        name: body.name,
	    description: body.description,
	    type: body.type,
	    imageUrl: body.imageUrl,
	    buyPrice: body.buyPrice,
	    promotionPrice: body.promotionPrice,
	    amount: body.amount
    }

    ProductModel.findByIdAndUpdate(Id,newProductDataUpdate,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message: "Sửa thành công dữ liệu thông qua Id",
            Data: data
        })
    })
}
// deleteProduct
const deleteProduct = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let Id = req.params.productId;
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
          message: 'id không hợp lệ'
        })
      }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    ProductModel.findByIdAndDelete(Id,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message: "xóa thành công dữ liệu thông qua Id",
            Data: data
        })
    })
}

//exprots thành modul
module.exports = {
    createProduct,
    getAllProduct,
    getProductById,
    updateProduct,
    deleteProduct
}