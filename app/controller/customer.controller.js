//Khai báo thư viện mongose
const mongoose = require("mongoose");
//Khai báo thư viện model
const customerModel = require("../model/customer.model");
const bcrypt = require('bcrypt');

// createCustomer 
const createCustomer = async (req, res) => {
    //B1 chuẩn bị dữ liệu
    let body = req.body
    console.log(body)
    //B2 Kiểm tra dữ liệu
    if (!body.firstName) {
        return res.status(400).json({
            message: "firstName không có"
        })
    }
    if (!body.lastName) {
        return res.status(400).json({
            message: "Last Name không có"
        })
    }
    if (!body.phone) {
        return res.status(400).json({
            message: "Phone không có"
        })
    }
    if (!body.email) {
        return res.status(400).json({
            message: "Email không có"
        })
    }
    if (!body.street) {
        return res.status(400).json({
            message: "street không có"
        })
    }
    if (!body.city) {
        return res.status(400).json({
            message: "City không có"
        })
    }
    if (!body.province) {
        return res.status(400).json({
            message: "province không có"
        })
    }
    if (!body.wards) {
        return res.status(400).json({
            message: "wards  không có"
        })
    }
    if(!body.password){
        return res.status(400).json({
            message: "password không được để trống"
        })
    }
   
    const salt = await bcrypt.genSaltSync(8)
    const hash = await bcrypt.hashSync(body.password,salt);
        //B3 Gọi model thực hiện nghiệp vụ
    let newCustomerData = {
        _id: mongoose.Types.ObjectId(),
        firstName: body.firstName,
        lastName: body.lastName,
        phone: body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        province: body.province,
        wards: body.wards,
        image: body.image,
        street: body.street,
        password: hash
    }
    customerModel.create(newCustomerData, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(201).json({
            message: "Tạo dữ liệu customer thành công",
            Data: data
        })
    })
    }


// getAllCustomer
const getAllCustomer = (req, res) => {
    //B1: thu thập dữ liệu từ req
    // let phone = req.body.phone
    // let condition = {}
    //B2 Kiểm tra dữ liệu
    // if(phone){
    //     condition.phone = phone
    // }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    customerModel.find().exec((err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message: "Tải toàn bộ customer thành công",
            Data: data
        })
    })
}
// getCustomerById
const getCustomerById = (req, res) => {
    //B1: thu thập dữ liệu từ req
    let Id = req.params.customerId
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            message: 'id không hợp lệ'
        })
    }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    customerModel.findById(Id, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message: "Lấy dữ liệu thành công thông qua Id",
            Data: data
        })
    })
}
// updateCustomer
const updateCustomer = (req, res) => {
    //B1: thu thập dữ liệu từ req
    let Id = req.params.customerId
    let body = req.body
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            message: 'id không hợp lệ'
        })
    }
    if (!body.firstName) {
        return res.status(400).json({
            message: err.message
        })
    }
    if (!body.lastName) {
        return res.status(400).json({
            message: err.message
        })
    }
    if (!body.phone) {
        return res.status(400).json({
            message: err.message
        })
    }
    if (!body.email) {
        return res.status(400).json({
            message: err.message
        })
    }
    if (!body.address) {
        return res.status(400).json({
            message: err.message
        })
    }
    if (!body.city) {
        return res.status(400).json({
            message: err.message
        })
    }
    if (!body.province) {
        return res.status(400).json({
            message: err.message
        })
    }
    if (!body.wards) {
        return res.status(400).json({
            message: err.message
        })
    }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    let newCustomerData = {
        firstName: body.firstName,
        lastName: body.lastName,
        phone: body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        province: body.province,
        wards: body.wards,
        image: body.image,
        street: body.street
    }
    customerModel.findByIdAndUpdate(Id, newCustomerData, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message: "Sửa dữ liệu thành công thông qua Id",
            Data: data
        })
    })
}
// deleteCustomer
const deleteCustomer = (req, res) => {
    //B1: thu thập dữ liệu từ req
    let Id = req.params.customerId;
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
          message: 'id không hợp lệ'
        })
      }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    customerModel.findByIdAndDelete(Id,(err,data)=>{
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message: "Xóa dữ liệu thành công thông qua Id",
            Data: data
        })
    })
}
//exprot thành modul
module.exports = {
    createCustomer,
    getAllCustomer,
    getCustomerById,
    updateCustomer,
    deleteCustomer
}