//Khai báo thư viện mongose
const mongoose = require("mongoose");

//Khai báo thư viện model
const orderModel = require("../model/order.model");
const customerModel = require("../model/customer.model");
// createOrderOfCustomer
const createOrderOfCustomer = (req, res) => {
    //B1 Chuẩn bị dữ liệu
    let Id = req.params.customerId
    let body = req.body
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            "status": "Bad Request",
            "Message": "customerId is not valid!"
        })
    }
    // if (!body.orderDate) {
    //     return res.status(400).json({
    //         message: "orderDate is not valid!"
    //     })
    // }
    if (!body.shippedDate) {
        return res.status(400).json({
            message: "shippedDate is not valid!"
        })
    }
    if (!body.note) {
        return res.status(400).json({
            message: "note is not valid!"
        })
    }
    if (!body.cost) {
        return res.status(400).json({
            message: "cost is not valid!"
        })
    }
    //B3 Gọi model thực hiện bào toán tạo mới dữ liệu
    let newOrderData = {
        _id: mongoose.Types.ObjectId(),
        // orderDate: body.orderDate,
        shippedDate: body.shippedDate,
        note: body.note,
        cost: body.cost
    }
    orderModel.create(newOrderData,(err,data)=>{
        if(err) {
            return res.status(500).json({
                status: "Internal server error2",
                message: err.message
            })
        }
         // Khi tạo order xong cần thêm id order mới vào mảng order của customer
         customerModel.findByIdAndUpdate(Id,{$push:{orders:data._id}},(err1,data1)=>{
            if(err1){
                return res.status(500).json({
                    status: "Internal server error1",
                    message: err1.message
                })
            }
            return res.status(201).json({
                status: "Create order successfully",
                Data: data
            })
         })
    })
}
// getAllOrder
const getAllOrder = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    //B2 Kiểm tra dữ liệu
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    orderModel.find((err,data)=>{
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message: "Tải toàn bộ order thành công",
            Data: data
        })
    })
}
// getAllOrderOfCustomer
const getAllOrderOfCustomer = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let Id = req.params.customerId
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            "status": "Bad Request",
            "Message": "customerId is not valid!"
        })
    }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    customerModel.findById(Id).populate("orders").exec((err,data)=>{
        if(err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        
        return res.status(200).json({
            status: "Get order successfully",
            Data: data.orders
        })
    })
}
// getOrderById
const getOrderById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let Id = req.params.orderId
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            "status": "Bad Request",
            "Message": "customerId is not valid!"
        })
    }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    orderModel.findById(Id,(err,data)=>{
        if(err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        
        return res.status(200).json({
            status: "Get order successfully",
            Data: data
        })
    })
}
// updateOrder
const updateOrder =(req,res)=>{
    //B1: thu thập dữ liệu từ req
    let Id = req.params.orderId;
    let body = req.body;
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            "status": "Bad Request",
            "Message": "customerId is not valid!"
        })
    }
    if (!body.orderDate) {
        return res.status(400).json({
            message: "orderDate is not valid!"
        })
    }
    if (!body.shippedDate) {
        return res.status(400).json({
            message: "shippedDate is not valid!"
        })
    }
    if (!body.note) {
        return res.status(400).json({
            message: "note is not valid!"
        })
    }
    if (!body.cost) {
        return res.status(400).json({
            message: "cost is not valid!"
        })
    }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    let newOrderUpdateData = {
        // orderDate: body.orderDate,
        shippedDate: body.shippedDate,
        note: body.note,
        cost: body.cost
    }
    orderModel.findByIdAndUpdate(Id,newOrderUpdateData,(err,data)=>{
        if(err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        
        return res.status(200).json({
            status: "Update order by Id successfully",
            Data: data
        })
    })
}
// deleteOrder
const deleteOrder = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let Id = req.params.orderId;
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            "status": "Bad Request",
            "Message": "customerId is not valid!"
        })
    }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    orderModel.findByIdAndDelete(Id,(err,data)=>{
        if(err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        
        return res.status(200).json({
            status: "Delete order by Id successfully",
            Data: data
        })
    })
}
//exprot thành modul
module.exports = {
    createOrderOfCustomer,
    getAllOrder,
    getAllOrderOfCustomer,
    getOrderById,
    updateOrder,
    deleteOrder
}