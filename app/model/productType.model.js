// import thư viện mongoose
const mongoose = require("mongoose");

// khai báo class schema của thư viện
const schema = mongoose.Schema; //giống như khai báo class trong ES6

// Khai báo ProductType Schema
const ProductTypeSchema = new schema({
    //Trường Id
    _id:{
        type: mongoose.Types.ObjectId,
        require: true
    },
    name:{
        type: String,
        unique: true,
        required: true
    },
    description:{
        type: String
    }
},{timestamps:true});


//exprot thành model
module.exports = mongoose.model("ProductType",ProductTypeSchema)