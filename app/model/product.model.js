// import thư viện mongoose
const mongoose = require("mongoose");

// khai báo class schema của thư viện
const schema = mongoose.Schema; //giống như khai báo class trong ES6

// Khai báo product schema
const productSchema = new schema({
    // trường id
    _id: {
        type: mongoose.Types.ObjectId,
        require: true
    },
    //trường name
    name: {
        type: String,
        unique: true,
        required: true
    },
    //Trường description
    description: {
        type: String
    },
    //Trường type là một oject
    type: {
        type:mongoose.Types.ObjectId,
        ref:"ProductType",
        require:true
    },
    //Trường imageUrl
    imageUrl:{
        type: String,
        require:true
    },
    //Trường buyPrice
    buyPrice:{
        type: Number,
        require:true
    },
    // Trường promotionPrice
    promotionPrice:{
        type:Number,
        require: true
    },
    // Trường amount
    amount:{
        type: Number,
        default: 0
    }
},{timestamps:true})

//exprot schema thành modul
module.exports = mongoose.model("Product", productSchema);