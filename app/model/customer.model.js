//Improt thư viện mongoose
const mongoose = require("mongoose");
//Khai báo classs schema của thư viện
const schema = mongoose.Schema;
//khai báo customerSchema
const customerSchema = new schema({
    // Trường id
    _id: {
        type: mongoose.Types.ObjectId,
        require: true
    },
    //Trường fristName
    firstName: {
        type: String,
        required: true
    },
    //Trường lastName
    lastName: {
        type: String,
        required: true
    },
    //Trường phone
    phone: {
        type: String,
        unique: true,
        required: true
    },
    //Trường Email
    email:{
        type: String,
        unique: true,
        required: true
    },
    //Trường street
    street:{
        type: String,
        default:""
    },
    //Trường city
    city:{
        type: String,
        default:""
    },
    // Trường province
    province:{
        type: String,
        default:""
    },
     // Trường wards
     wards:{
        type: String,
        default:""
    },
    // Trường IMG
    image:{
        type:String,
        default:""
    },
    //Trường Street
    street:{
        type:String,
        default:""
    },
    password:{
        type:String,
        default:"",
        require:true
    },
    admin:{
        type:Boolean,
        default:false
    },
    // orders
    orders:[
        {
            type: mongoose.Types.ObjectId,
            ref:"Order"
        }
    ]
},{timestamps:true})

//exprot thành modul
module.exports = mongoose.model("Customer",customerSchema)