//Improt thư viện mongoose
const mongoose = require("mongoose");
//Khai báo classs schema của thư viện
const schema = mongoose.Schema;
//khai báo orderSchema
const orderSchema = new schema({
    //Trường Id
    _id: {
        type: mongoose.Types.ObjectId,
        require: true
    },
    //Trường orderDate
    orderDate: {
        type: Date,
        default: Date.now()
    },
    // Trường shippedDate
    shippedDate: {
        type: Date
    },
    // Trường note
    note: {
        type: String
    },
    // Trường orderDetails
    orderDetails: [
        {
            type: mongoose.Types.ObjectId,
            ref: "OrderDetail"
        }
    ], 
    // Trường cost
    cost: {
        type: Number,
        default: 0
    }
}, { timestamps: true });
//exprot thành modul
module.exports = mongoose.model("Order", orderSchema);