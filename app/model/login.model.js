//Improt thư viện mongoose
const mongoose = require("mongoose");
//Khai báo classs schema của thư viện
const schema = mongoose.Schema;
// Khai báo loginSchema
const loginSchema= new schema({
    // Trường id
    _id: {
        type: mongoose.Types.ObjectId,
        require: true
    },
    //email
    email:{
        type: String,
        required: true
    },
    //trường password
    password:{
        type:String,
        default:""
    }
},{timestamps:true})
//exprot thành modul
module.exports = mongoose.model("login",loginSchema)