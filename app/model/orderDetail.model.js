//Improt thư viện mongoose
const mongoose = require("mongoose");
//Khai báo classs schema của thư viện
const schema = mongoose.Schema;
//khai báo orderDetailSchema
const orderDetailSchema = new schema({
    //Trường Id
    _id: {
        type: mongoose.Types.ObjectId,
        require: true
    },
    // Trường product
    product:{
        type: mongoose.Types.ObjectId,
        ref: "Product"
    },
    // Trường quantity
    quantity:{
        type: Number,
        default:0
    }
})
//export thành modul
module.exports = mongoose.model("OrderDetail", orderDetailSchema);