const express = require("express"); // Tương tự : import express from "express";
const path = require("path"); // thư viện đường dẫn
const bodyParser = require('body-parser');
const dotenv = require("dotenv");
const cors = require("cors");
const cookieParser = require("cookie-parser");
// Khởi tạo Express App
const app = express();

const port = 8000;

// import thư viện mogoose
const mongoose = require("mongoose");

//Nơi khai báo router
const {productTypeRouter} = require("./app/router/productType.router");
const { productRouter } = require("./app/router/product.router");
const { customerRouter } = require("./app/router/customer.router");
const { orderRouter } = require("./app/router/order.router");
const { orderDetailRouter } = require("./app/router/orderDetail.router");
const { logInRouter } = require("./app/router/login.router");


//khai báo tránh lỗi Access-Control-Allow-Origin do CORS Middleware validate liên quan đến vấn đề bảo mật 
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
// Gửi yêu cầu phân tích kiểu nội dung application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
// Gửi yêu cầu phân tích kiểu nội dung application/json
app.use(bodyParser.json())
app.use(cors())
app.use(cookieParser())
//sử dụng được body json
app.use(express.json());
//sử dụng body unicode
app.use(express.urlencoded({
    extended:true
}))

mongoose.set('strictQuery', true);

// Kết nối với mongodb
mongoose.connect(`mongodb://127.0.0.1:27017/Shop24hV01`, function(error) {
    if (error) throw error;
    console.log('Kết nối thành công với mongoDB');
   })


// Nơi sử dụng các Router
app.use(productTypeRouter);
app.use(productRouter);
app.use(customerRouter);
app.use(orderRouter);
app.use(orderDetailRouter);
app.use(logInRouter);


   app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})